#include "RestrictedBoltzmannMachine.cuh"
#include "Utility.cuh"


int main(int argc, const char** argv) {
	int visible = 5;
	int hidden = 50;
	int samples = 10;
	int miniBatchSize = 3;
	double lambda = 0;
	double lambdaFactor = 0;
	double momentum = 0;
	double momentumFactor = 0;
	int steps = 10;
	int stepsIncremental = 0;
	int epochs = 5;
	int epochsFactorUpdate = 0;
	double learningRate = 0.001;
	double learningRateFactor = 0; 
	

	// Random data
	double* data = new double[visible*samples];	
	double* weights = new double[visible*hidden];
	double* biasesHidden = new double[hidden];
	double* biasesVisible = new double[visible];
	
	for(int i = 0; i < samples*visible; i++)
		data[i] = i / 100;

	for(int i = 0; i < hidden*visible; i++) {
		weights[i] = i / 100;
		if (i < hidden)
			biasesHidden[i] = i / 100;
		if (i < visible)
			biasesVisible[i] = i / 100;
	}
	// Train model (full batch)
	LinearBinaryRestrictedBoltzmannMachine rbm = LinearBinaryRestrictedBoltzmannMachine(weights, biasesVisible, biasesHidden, hidden, visible);
	rbm.TrainNetwork(data, samples, miniBatchSize, learningRate, learningRateFactor, epochs, epochsFactorUpdate, steps, stepsIncremental, momentum, momentumFactor, lambda, lambdaFactor);
	
	// End
	delete[] data;
	return 0;
}
