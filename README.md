## Configuration

```
sudo cp $MATLABROOT/toolbox/distcomp/gpu/extern/src/mex/glnxa64/nvcc_g++.xml $MATLABROOT/bin/glnxa64/mexopts
sudo cp $MATLABROOT/toolbox/distcomp/gpu/extern/src/mex/glnxa64/nvcc_g++_dynamic.xml $MATLABROOT/bin/glnxa64/mexopts
mex -setup nvcc_g++
```

Add changes to the following flags: ```~/.matlab/version/mex_CUDA_glnxa64.xml```:
```
NVCCFLAGS+="--expt-extended-lambda"
LINKLIBS+="-lcublas -lcurand" 
```

```
sudo cp $CUDA_HOME/lib64/libcublas.so $MATLABROOT/bin/glnxa64/libcublas.so
sudo cp $CUDA_HOME/lib64/libcublas.so.7.5 $MATLABROOT/bin/glnxa64/libcublas.so.7.5
sudo cp $CUDA_HOME/lib64/libcublas.so.7.5.18 $MATLABROOT/bin/glnxa64/libcublas.so.7.5.18
sudo cp $CUDA_HOME/lib64/libcurand.so $MATLABROOT/bin/glnxa64/libcurand.so
sudo cp $CUDA_HOME/lib64/libcurand.so.7.5 $MATLABROOT/bin/glnxa64/libcurand.so.7.5
sudo cp $CUDA_HOME/lib64/libcurand.so.7.5.18 $MATLABROOT/bin/glnxa64/libcurand.so.7.5.18
```

```
nvcc main.cu RestrictedBoltzmannMachine.cu Utility.cu --std=c++11 --expt-extended-lambda -lcublas
```


