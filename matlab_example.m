data = [1 5 9;
        2 6 10;
        3 7 11;
        4 8 12];

nVisible = size(data,2);

mode = 'LinearNRelu';
nHidden = 10;
miniBatchSize = 2;
learningRate = 0.1;
learningRateFactor = 10;
cdSteps = 1;
cdStepsIncremental = 0;
epochs = 1;
epochsFactorUpdate = 0; 
momentum = 0.0;
momentumFactor = 0;
lambda = 0.0;
lambdaFactor = 0;

weights = reshape((1:30)./ 100, nHidden, nVisible);
visibleBiases = 1:3;
hiddenBiases = 1:10;

[weightsNew, visibleBiasesNew, hiddenBiasesNew] = mexTrainRBM(data, ...
    weights, visibleBiases, hiddenBiases, mode, nHidden, miniBatchSize, ...
    learningRate, learningRateFactor, cdSteps, cdStepsIncremental, epochs, ... 
    epochsFactorUpdate, momentum, momentumFactor, lambda, lambdaFactor);